# Flutter Test App

A simple Flutter app to test various functionalities, including:

1. Drawer
2. SharedPreferences
3. Theme
4. sqflite (insert, delete, select, transaction, batch)
5. Dialogs
6. File read/write
7. Form
