import 'package:flutter/material.dart';
import 'package:flutter_test_app/screens/start.dart';
import 'package:flutter_test_app/screens/networking.dart';
import 'package:flutter_test_app/screens/persistence.dart';
import 'package:flutter_test_app/screens/form.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Ishan Fernando'),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          ),
          ListTile(
            title: Text('Start'),
            onTap: () {
              openScreen(context, StartScreen());
            },
          ),
          ListTile(
            title: Text('Persistence'),
            onTap: () {
              openScreen(context, PersistenceScreen());
            },
          ),
          ListTile(
            title: Text('Networking'),
            onTap: () {
              openScreen(context, NetworkingScreen());
            },
          ),
          ListTile(
            title: Text('Form'),
            onTap: () {
              openScreen(context, FormScreen());
            },
          ),
        ],
      ),
    );
  }

  void openScreen(BuildContext context, Widget screen) {
    Navigator.pop(context);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => screen),
    );
  }
}
