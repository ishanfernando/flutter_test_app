import 'package:flutter/material.dart';

Padding addPadding(Widget widget, double padding) {
  return Padding(
    padding: EdgeInsets.all(padding),
    child: widget,
  );
}

void showBusyDialog(BuildContext context, String message) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            addPadding(CircularProgressIndicator(), 20.0),
            Text(message),
          ],
        ),
      );
    },
  );
}

void showInfoDialog(BuildContext context, String title, String message) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: new Text(title),
        content: new Text(message),
        actions: <Widget>[
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
