import 'package:flutter/material.dart';
import 'package:flutter_test_app/screens/start.dart';

void main() => runApp(FlutterTestApp());

class FlutterTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.deepOrange,
      ),
      home: StartScreen(),
    );
  }
}
