import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DatabaseClient {

  Database _db;

  Future create() async {
    Directory path = await getApplicationDocumentsDirectory();
    String dbPath = join(path.path, 'database.db');
    _db = await openDatabase(dbPath, version: 1, onCreate: _create);
  }

  Future _create(Database db, int version) async {
    await db.execute("""
      CREATE TABLE item (
        item_number TEXT PRIMARY KEY,
        name TEXT NOT NULL,
        quantity INTEGER NOT NULL,
        price REAL NOT NULL,
        active INTEGER NOT NULL
      )
    """);
  }

  Future insertItem(Map<String, dynamic> item) async {
    await _db.transaction((txn) async {
      await txn.insert('item', item);
    });
  }

  Future insertItems(List<Map<String, dynamic>> items) async {
    await _db.transaction((txn) async {
      var batch = txn.batch();
      for(var item in items){
        batch.insert('item', item);
      }
      await batch.commit(noResult: true);
    });
  }

  Future<int> countItems() async {
    return Sqflite
        .firstIntValue(await _db.rawQuery("SELECT COUNT(*) FROM item"));
  }

  Future<int> deleteAllItems() async {
    int count;
    await _db.transaction((txn) async {
      count = await txn.rawDelete('DELETE FROM item');
    });
    return count;
  }
}
