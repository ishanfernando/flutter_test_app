import 'package:flutter/material.dart';
import 'package:flutter_test_app/widgets/custom_drawer.dart';
import 'package:flutter_test_app/utils/ui_utils.dart' as uiUtils;

class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class _FormData {
  String itemNumber;
  bool active = false;
  String type;
  @override
  String toString() {
    return 'itemNumber=$itemNumber\nactive=$active\ntype=$type';
  }
}

class FormScreenState extends State<FormScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  _FormData _data = _FormData();

  @override
  Widget build(BuildContext context) {
    var items = ['Type 1', 'Type 2', 'Type 3']
        .map((String s) => DropdownMenuItem<String>(
              value: s,
              child: Text(s),
            ))
        .toList();

    return Scaffold(
        drawer: CustomDrawer(),
        appBar: AppBar(
          title: Text('FTA - Form'),
        ),
        body: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              uiUtils.addPadding(
                  Row(
                    children: <Widget>[
                      SizedBox(width: 100.0, child: Text('Item number: ')),
                      Expanded(child: TextFormField(
                        onSaved: (String value) {
                          _data.itemNumber = value;
                        },
                      ))
                    ],
                  ),
                  10.0),
              uiUtils.addPadding(
                  Row(
                    children: <Widget>[
                      SizedBox(width: 100.0, child: Text('Active: ')),
                      Switch(
                          value: _data.active,
                          onChanged: (bool value) {
                            setState(() {
                              _data.active = value;
                            });
                          })
                    ],
                  ),
                  10.0),
              uiUtils.addPadding(
                  Row(
                    children: <Widget>[
                      SizedBox(width: 100.0, child: Text('Type: ')),
                      DropdownButton(
                        value: _data.type,
                        items: items,
                        hint: Text('Select a type'),
                        onChanged: (String value) {
                          setState(() {
                            _data.type = value;
                          });
                        },
                      ),
                    ],
                  ),
                  10.0),
              Align(
                alignment: Alignment.centerRight,
                child: uiUtils.addPadding(
                    RaisedButton(
                      child: Text('SHOW'),
                      onPressed: () {
                        _formKey.currentState.save();
                        uiUtils.showInfoDialog(
                            context, "Form", _data.toString());
                      },
                    ),
                    10.0),
              ),
            ],
          ),
        ));
  }
}
