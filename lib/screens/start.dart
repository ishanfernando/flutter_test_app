import 'package:flutter/material.dart';
import 'package:flutter_test_app/widgets/custom_drawer.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(
        title: Text('FTA - Start'),
      ),
      body: Center(
        child: Text('Start screen'),
      ),
    );
  }
}
