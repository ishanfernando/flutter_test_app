import 'package:flutter/material.dart';
import 'package:flutter_test_app/widgets/custom_drawer.dart';
import 'package:flutter_test_app/utils/ui_utils.dart' as uiUtils;
import 'package:flutter_test_app/http/http_client.dart' as http;

class NetworkingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(
        title: Text('FTA - Networking'),
      ),
      body: Column(
        children: <Widget>[
          uiUtils.addPadding(Text('http get test'), 10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('GET DATA'),
                onPressed: () {
                  getData(context);
                },
              ),
              10.0),
          Divider(),
        ],
      ),
    );
  }

  void getData(BuildContext context) async {
    List<dynamic> posts = await http.getPosts();
    uiUtils.showInfoDialog(context, 'http get', 'Got ${posts.length} data');
  }
}
