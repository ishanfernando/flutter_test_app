import 'package:flutter/material.dart';
import 'package:flutter_test_app/utils/ui_utils.dart' as uiUtils;
import 'package:flutter_test_app/widgets/custom_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_test_app/db/db_client.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class PersistenceScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      appBar: AppBar(
        title: Text('FTA - Persistence'),
      ),
      body: Column(
        children: <Widget>[
          uiUtils.addPadding(Text('SharedPreferences test'), 10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('INCREMENT VALUE'),
                onPressed: _increment,
              ),
              10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('SHOW VALUE'),
                onPressed: () {
                  _show(context);
                },
              ),
              10.0),
          Divider(),
          uiUtils.addPadding(Text('sqflite test'), 10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('INSERT RECORDS'),
                onPressed: () {
                  _insertRecords(context);
                },
              ),
              10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('READ RECORDS'),
                onPressed: () {
                  _readRecords(context);
                },
              ),
              10.0),
          Divider(),
          uiUtils.addPadding(Text('file test'), 10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('WRITE TIME TO FILE'),
                onPressed: () {
                  _writeTimeToFile();
                },
              ),
              10.0),
          uiUtils.addPadding(
              RaisedButton(
                child: Text('READ TIME FROM FILE'),
                onPressed: () {
                  _readTimeFromFile(context);
                },
              ),
              10.0),
          Divider(),
        ],
      ),
    );
  }

  void _increment() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int value = prefs.getInt('value') ?? 0;
    prefs.setInt('value', value + 1);
  }

  void _show(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int value = prefs.getInt('value') ?? 0;
    uiUtils.showInfoDialog(
        context, 'SharedPreferences', 'Current value is ${value.toString()}');
  }

  void _insertRecords(BuildContext context) async {
    uiUtils.showBusyDialog(context, 'Inserting...');

    Stopwatch watch = Stopwatch();
    watch.start();

    DatabaseClient db = new DatabaseClient();
    await db.create();

    var deleteCount = await db.deleteAllItems();
    print('Deleted $deleteCount items');

    List<Map<String, dynamic>> valuesList = List();
    for (int i = 1; i <= 200001; i++) {
      Map<String, dynamic> values = {
        'item_number': i.toString(),
        'name': 'item ${i.toString()}',
        'quantity': 1,
        'price': 1.5,
        'active': 1,
      };
      valuesList.add(values);
      if (valuesList.length == 5000) {
        await db.insertItems(valuesList);
        print('Inserted ${valuesList.length} items');
        valuesList.clear();
      }
    }
    if (valuesList.isNotEmpty) {
      await db.insertItems(valuesList);
      print('Inserted ${valuesList.length} items');
      valuesList.clear();
    }

    watch.stop();
    Navigator.pop(context); // close progress dialog

    var count = await db.countItems();
    print('Inserted ${count.toString()} items in ${watch.elapsed.inSeconds} s');

    uiUtils.showInfoDialog(context, 'sqflite',
        'Inserted ${count.toString()} items in ${watch.elapsed.inSeconds} s');
  }

  void _readRecords(BuildContext context) async {
    DatabaseClient db = new DatabaseClient();
    await db.create();
    var count = await db.countItems();
    print('There are ${count.toString()} items');
    uiUtils.showInfoDialog(
        context, 'sqflite', 'There are ${count.toString()} items');
  }

  void _writeTimeToFile() async {
    Directory dir = await getApplicationDocumentsDirectory();
    File file = File('${dir.path}/time.txt');
    await file.writeAsString(DateTime.now().toString());
  }

  void _readTimeFromFile(BuildContext context) async {
    Directory dir = await getApplicationDocumentsDirectory();
    File file = File('${dir.path}/time.txt');
    if (await file.exists()) {
      uiUtils.showInfoDialog(
          context, 'file', 'Time is ${await file.readAsString()}');
    } else {
      uiUtils.showInfoDialog(context, 'file', 'File not found');
    }
  }
}
