import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<dynamic>> getPosts() async {
  var resp = await http.get('https://jsonplaceholder.typicode.com/posts');
  if(resp.statusCode == 200){ // OK
    return json.decode(resp.body);
  } else {
    throw Exception('Failed to get posts');
  }
}